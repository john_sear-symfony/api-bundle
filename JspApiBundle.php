<?php declare(strict_types=1);

namespace JohnSear\JspApiBundle;

use JohnSear\JspApiBundle\DependencyInjection\JspApiExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class JspApiBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new JspApiExtension();
    }
}

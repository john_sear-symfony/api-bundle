<?php declare(strict_types=1);

namespace JohnSear\JspApiBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Throwable;

class ExceptionListener implements EventSubscriberInterface
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $request = $event->getRequest();

        $hasBearer = (
            $request->headers->has('authorization') &&
            is_string($request->headers->get('authorization')) &&
            strpos($request->headers->get('authorization'), 'Bearer ') !== false
        );
        $hasAuthToken = $request->headers->has('X-AUTH-TOKEN');

        $isApiRoute = (is_string($request->attributes->get('_route')) && strpos($request->attributes->get('_route'), 'api_') !== false);

        if ($hasBearer || $hasAuthToken || $isApiRoute) {
            /** @var Throwable $ex */
            $ex = $event->getThrowable();

            $statusCode = ($ex instanceof HttpException) ? $ex->getStatusCode() : Response::HTTP_EXPECTATION_FAILED;

            (new JsonResponse(['error_message' => $ex->getMessage()], $statusCode))->send();

            die();
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [KernelEvents::EXCEPTION => 'onKernelException'];
    }
}

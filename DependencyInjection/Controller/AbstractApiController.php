<?php declare(strict_types=1);

namespace JohnSear\JspApiBundle\DependencyInjection\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as SymfonyAbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

abstract class AbstractApiController extends SymfonyAbstractController
{
    protected function json($data, int $status = 200, array $headers = [], array $context = []): JsonResponse
    {
        $preparedData = $this->prepareDataForJsonResponse($data);

        return parent::json($preparedData, $status, $headers, $context);
    }

    /**
     * @param mixed $data Data to get the serializer for
     */
    protected function prepareDataForJsonResponse($data): array
    {
        $serializer = $this->getSerializer();

        $format = ['ignored_attributes' => [

            /**
             * Ignore not needed UserAccount Attributes
             */

            'salt',
            'password',
            'encodedPassword',
            'roles',


            /**
             * Ignore not needed DateTime Attributes
             */

            'timezone',
            'offset',


            /**
             * Ignore not needed LoggingEntity Attributes
             */

            'currentUser',
        ]];

        /** @TODO: Refactoring so $this->json($data) returning full depth of data inheritance, without serialize and re-decode data first ... */
        $dataSerialized = $serializer->serialize($data, 'json', $format);
        return json_decode($dataSerialized, true);
    }
    
    protected function getSerializer(): SerializerInterface
    {
        $normalizer = new ObjectNormalizer();
        $encoder    = new JsonEncoder();

        return new Serializer([$normalizer], [$encoder]);
    }
}

<?php declare(strict_types=1);

namespace JohnSear\JspApiBundle\DependencyInjection;

use JohnSear\JspApiBundle\DependencyInjection\Controller\AbstractApiController;
use JohnSear\JspApiBundle\EventListener\ExceptionListener;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\BadMethodCallException;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class JspApiExtension extends Extension
{
    /** @var ContainerBuilder */
    private $container;

    /**
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $this->container = $container;

        $this->registerExceptionListener();
        $this->registerApiController();
    }

    /**
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    private function registerExceptionListener(): void
    {
        $definition = new Definition(ExceptionListener::class, []);

        $this->container->setDefinition('jsp.api.exception_listener', $definition)
            ->addTag('kernel.event_subscriber', ['event' => 'kernel.exception'])
            ->setPublic(true);
        $this->container->setAlias(ExceptionListener::class, 'jsp.api.exception_listener')
            ->setPublic(true);
    }

    /**
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    private function registerApiController(): void
    {
        $definition = new Definition(AbstractApiController::class);
        $this->container->setDefinition('jsp.api.api_controller', $definition)
            ->addTag('controller.service_arguments')
            ->setPublic(true);
        $this->container->setAlias(AbstractApiController::class, 'jsp.api.api_controller')
            ->setPublic(true);
    }
}

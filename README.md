# ApiBundle
(c) 2019 by [John_Sear](https://bitbucket.org/john_sear-symfony/api-bundle/)

## Documentation
Documentation, i.e. installation with Composer, can be found [here](Resources/doc/index.md)
